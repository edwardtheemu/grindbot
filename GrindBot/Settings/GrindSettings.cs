﻿namespace GrindBot.Settings
{
    public class GrindSettings
    {

        public volatile int RestManaAt = 40;
        public volatile int RestHealthAt = 40;
        public volatile float MobSearchRange = 20;
        public volatile float MaxDiffToWp = 100;
        public volatile float CombatDistance = 4;

        public volatile int MinFreeSlotsBeforeVendor = 3;
        public volatile int KeepItemsFromQuality = 2;
        public volatile string[] ProtectedItems = { "" };
        public volatile float WaypointModifier = 0.4f;

        public volatile string LastProfile = "";

        public volatile bool StopOnRare = false;
        public volatile bool NotifyOnRare = false;

        public volatile int ForceBreakAfter = 0;
        public volatile int BreakFor = 0;


        public volatile bool BeepOnWhisper = false;
        public volatile bool BeepOnSay = false;
        public volatile bool BeepOnName = false;


        public volatile bool SkinUnits = false;
        public volatile bool NinjaSkin = false;
        public volatile bool LootUnits = true;
        public volatile bool Herb = false;
        public volatile bool Mine = false;


        public volatile float RezzDistanceToHostile = 17;
        public volatile float MaxResurrectDistance = 28;
        public volatile float CtmStopDistance = 3;

        public volatile float RepairDurabilityPercentage = 30;
        public volatile float MaxRandomTargetDistance = 30;
        public volatile float MaxResourceDistance = 35;

        public volatile bool ForceReturnToPath = true;
        public volatile int TargetSearchWait = 2000;

        private GrindSettings()
        {

        }

        internal static GrindSettings Values { get; set; } = new GrindSettings();

        internal void Load()
        {
            //If the .json file doesn't exist use default values.
            Values = ZzukBot.Settings.OptionManager.Get("GrindSettings").LoadFromJson<GrindSettings>() ??
                     this;
        }

        internal void Save()
        {
            ZzukBot.Settings.OptionManager.Get("GrindSettings").SaveToJson(this);
        }

    }
}

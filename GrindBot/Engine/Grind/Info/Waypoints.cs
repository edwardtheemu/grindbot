﻿using System;
using GrindBot.Engine.ProfileCreation;
using GrindBot.Settings;
using ZzukBot.Helpers.PPather;
using ZzukBot.Game.Statics;
using ZzukBot.Mem;

namespace GrindBot.Engine.Grind.Info
{
    internal class _Waypoints
    {
        private Action LoadFirstWaypointCallback;

        internal _Waypoints()
        {
            CurrentWaypointIndex = 0;
            CurrentHotspotIndex = ClosestHotspotIndex;
        }

        // index of the current waypoint
        internal int CurrentWaypointIndex { get; private set; }
        // index of the current hotspot
        internal int CurrentHotspotIndex { get; set; }
        // list of waypoints generated from player to hotspot
        internal ZzukBot.Helpers.PPather.Path CurrentWaypoints { get; set; }

        /// <summary>
        ///     Object of our current hotspot
        /// </summary>
        internal Location CurrentHotspot => Grinder.Access.Profile.Hotspots[CurrentHotspotIndex].Position;

        /// <summary>
        ///     Object of our current waypoint
        /// </summary>
        internal Location CurrentWaypoint => CurrentWaypoints.Get(CurrentWaypointIndex);

        /// <summary>
        ///     Did we arrive at the last waypoint?
        /// </summary>
        internal bool AtLastWaypoint
        {
            get
            {
                // Pathing returning null (Ppather bug)
                if (CurrentWaypoints.Equals(null))
                    return true;
                return CurrentWaypointIndex == CurrentWaypoints.Count() - 1 && NeedToLoadNextWaypoint(1.8f);
            }
        } 

        // Distance to current waypoint

        private int ClosestHotspotIndex
        {
            get
            {
                var closestIndex = -1;
                for (var i = CurrentWaypointIndex; i < Grinder.Access.Profile.Hotspots.Length; i++)
                {
                    if (closestIndex == -1)
                    {
                        closestIndex = 0;
                        continue;
                    }
                    var disA =
                        ObjectManager.Instance.Player.Position.GetDistanceTo(
                            Grinder.Access.Profile.Hotspots[closestIndex].Position);

                    var disB =
                        ObjectManager.Instance.Player.Position.GetDistanceTo(Grinder.Access.Profile.Hotspots[i].Position);
                    if (disB < disA)
                    {
                        closestIndex = i;
                    }
                }
                return closestIndex;
            }
        }

        /// <summary>
        ///     Set current waypoint to the closest
        /// </summary>
        internal void SetCurrentWaypointToClosest()
        {
            var closestIndex = CurrentWaypointIndex;
            for (var i = CurrentWaypointIndex; i < CurrentWaypoints.Count(); i++)
            {
                var disA = ObjectManager.Instance.Player.Position.GetDistanceTo(CurrentWaypoints.Get(closestIndex));
                var disB = ObjectManager.Instance.Player.Position.GetDistanceTo(CurrentWaypoints.Get(i));


                if (disB < disA)
                {
                    closestIndex = i;
                }
            }
            CurrentWaypointIndex = closestIndex;
        }

        internal int GetClosestHotspotIndex()
        {
            var closestIndex = CurrentHotspotIndex;
            for (var i = 0; i < Grinder.Access.Profile.Hotspots.Length; i++)
            {
                var disA = ObjectManager.Instance.Player.Position.GetDistanceTo(Grinder.Access.Profile.Hotspots[closestIndex].Position);
                var disB = ObjectManager.Instance.Player.Position.GetDistanceTo(Grinder.Access.Profile.Hotspots[i].Position);


                if (disB < disA)
                {
                    closestIndex = i;
                }
            }
            return closestIndex;
        }

        /// <summary>
        ///     Load the next hotspot if we reached the end of our current waypoints
        /// </summary>
        internal void LoadNextHotspot()
        {
            var tmp = CurrentHotspotIndex + 1;
            if (tmp < Grinder.Access.Profile.Hotspots.Length)
                CurrentHotspotIndex = tmp;
            else
            {
                Array.Reverse(Grinder.Access.Profile.Hotspots);
                CurrentHotspotIndex = 1;
            }
            // load the next set of waypoints and set the index to 0
            Grinder.Access.Info.Waypoints.LoadWaypoints();
        }

        /// <summary>
        ///     Load the next waypoint in lsit if we reached the current one
        /// </summary>
        internal void LoadNextWaypoint()
        {
            CurrentWaypointIndex =
                (CurrentWaypointIndex + 1) % CurrentWaypoints.Count();
        }

        /// <summary>
        ///     Load waypoints from player to current hotspot
        /// </summary>
        internal void LoadWaypoints()
        {
            var pos = ObjectManager.Instance.Player.Position;
            CurrentWaypoints = Navigation.Instance.CalculatePath(pos, CurrentHotspot);
            CurrentWaypointIndex = 1;
        }

        internal void LoadFirstWaypointsAsync(Action Callback)
        {
            var pos = ObjectManager.Instance.Player.Position;
            LoadFirstWaypointCallback = Callback;

            //Navigation.CalculatePathAsync(pos, CurrentHotspot, true, LoadFirstCallBack);
        }

        private void LoadFirstCallBack(ZzukBot.Helpers.PPather.Path parPath)
        {
            CurrentWaypoints = parPath;
            CurrentWaypointIndex = 1;
            LoadFirstWaypointCallback();
        }

        /// <summary>
        ///     Are we close enough to the current waypoint? If yes load next
        /// </summary>
        internal bool NeedToLoadNextWaypoint(Location parPoint, float disToPoint = 1.3f, bool twoD = false)
        {
            var dis = ObjectManager.Instance.Player.Position.GetDistanceTo(parPoint);
            if (twoD)
                dis = ObjectManager.Instance.Player.Position.GetDistanceTo2D(parPoint);
            return dis <= disToPoint + GrindSettings.Values.WaypointModifier;
        }

        internal bool NeedToLoadNextWaypoint(float disToPoint = 1.3f)
        {
            var dis = ObjectManager.Instance.Player.Position.GetDistanceTo(CurrentWaypoint);
            return dis <= disToPoint + GrindSettings.Values.WaypointModifier;
        }


        internal void ResetGrindPath()
        {
            CurrentHotspotIndex = 0;
            CurrentWaypointIndex = 0;
            RevertHotspotsToOriginal();
        }

        internal void RevertHotspotsToOriginal()
        {
            Grinder.Access.Profile.Hotspots = Grinder.Access.Profile.OriginalHotspots;
        }
    }
}

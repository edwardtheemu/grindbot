﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GrindBot.Settings;
using ZzukBot.Game.Statics;

namespace GrindBot.Engine.Grind.Info
{
    internal class _BreakHelper
    {
        private bool _NeedToBreak;
        private int BreakAt;

        private readonly Random ran = new Random();
        private int ResumeAt;
        private bool SetResumeTime;

        internal _BreakHelper()
        {
            BreakAt = 0;
            ResumeAt = 0;
            _NeedToBreak = false;
            SetResumeTime = false;
        }

        internal bool NeedToBreak
        {
            get
            {
                if (!(GrindSettings.Values.BreakFor != 0 && GrindSettings.Values.ForceBreakAfter != 0)) return false;
                if (_NeedToBreak)
                {
                    if (!ObjectManager.Instance.IsIngame)
                    {
                        if (SetResumeTime)
                        {
                            SetResumeAt(0);
                            SetResumeTime = false;
                        }
                        else if (NeedToResume)
                        {
                            SetBreakAt(240000);
                            _NeedToBreak = false;
                        }
                    }
                    return true;
                }
                if (Environment.TickCount > BreakAt)
                {
                    _NeedToBreak = true;
                    return true;
                }
                SetResumeTime = true;
                return false;
            }
        }

        private bool NeedToResume => Environment.TickCount > ResumeAt;

        internal void SetBreakAt(int parModifier)
        {
            if (GrindSettings.Values.ForceBreakAfter < 5)
                GrindSettings.Values.ForceBreakAfter = 5;

            BreakAt = Environment.TickCount + GrindSettings.Values.ForceBreakAfter * 60 * 1000
                      + ran.Next(-120000, 120000) + parModifier;
        }

        private void SetResumeAt(int parModifier)
        {
            if (GrindSettings.Values.BreakFor < 5)
                GrindSettings.Values.BreakFor = 5;

            ResumeAt = Environment.TickCount + GrindSettings.Values.BreakFor * 60 * 1000
                       + ran.Next(-120000, 120000) + parModifier;
        }
    }
}

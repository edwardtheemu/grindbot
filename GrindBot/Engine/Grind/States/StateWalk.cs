﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Grindbot.Objects;
using GrindBot.Constants;
using GrindBot.Settings;
using ZzukBot.Game.Statics;

namespace GrindBot.Engine.Grind.States
{
    internal class StateWalk : State
    {
        internal Random ran = new Random();

        private int then = Environment.TickCount;

        internal override int Priority => 10;

        internal override bool NeedToRun => (((ObjectManager.Instance.Player.MovementState &
                                               (int)Enums.MovementFlags.Front)
                                              != (int)Enums.MovementFlags.Front)
                                             || !Grinder.Access.Info.Waypoints.NeedToLoadNextWaypoint())
                                            && ObjectManager.Instance.Player.Casting == 0
                                            && ObjectManager.Instance.Player.Channeling == 0;

        internal override string Name => "Walking";

        internal override void Run()
        {
            //Grinder.Access.PrintToChat("Walking.");
            //Console.WriteLine("Took " + (Environment.TickCount - then)+"ms to get back to walk.");
            then = Environment.TickCount;
            // start movement to the current waypoint
            if (ObjectManager.Instance.Player.Casting != 0)
                return;

            Shared.RandomJump();
            Grinder.Access.Info.PathAfterFightToWaypoint.AdjustPath();

            // ReSharper disable once ConvertIfStatementToConditionalTernaryExpression
            try
            {
                if (Grinder.Access.Info.PathAfterFightToWaypoint.AfterFightMovement)
                {
                    if (GrindSettings.Values.ForceReturnToPath || Grinder.Access.Info.Target.NextTarget == null)
                    {
                        ObjectManager.Instance.Player.CtmTo(
                            Grinder.Access.Info.PathToPosition.ToPos(Grinder.Access.Info.Waypoints.CurrentWaypoint));
                    }
                }
                else
                {
                    ObjectManager.Instance.Player.CtmTo(Grinder.Access.Info.Waypoints.CurrentWaypoint);
                }
            }
            catch //Path generation fails
            {
                //Reload Paths
                Grinder.Access.PrintToChat("Path Genertation failure - Rebuilding.");
                Grinder.Access.Info.Waypoints.LoadWaypoints();
            }

        }
    }
}

﻿using Grindbot.Objects;
using ZzukBot.ExtensionFramework;
using ZzukBot.Game.Statics;
using ZzukBot.Helpers;


namespace GrindBot.Engine.Grind.States
{
    internal class StateBuff : State
    {
        internal override int Priority => 44;

        internal override bool NeedToRun
        {
            get
            {
                if (!Wait.For("BuffCheck", 1000)) return false;
                return  !CustomClasses.Instance.Current.OnBuff();

            }
        }

        internal override string Name => "Buffing";

        private void ClearTarget()
        {
            var guid = ObjectManager.Instance.Player.Guid;
            var tarGuid = ObjectManager.Instance.Player.TargetGuid;
            if (tarGuid != 0
                &&
                tarGuid != guid && ObjectManager.Instance.Player.HasPet && tarGuid != ObjectManager.Instance.Pet.Guid)
            {
                ObjectManager.Instance.Player.SetTarget(guid);
            }
        }

        internal override void Run()
        {
            ClearTarget();
            ObjectManager.Instance.Player.CtmStopMovement();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZzukBot.Game.Statics;
using ZzukBot.Helpers.PPather;

namespace GrindBot.Engine.Grind
{
    internal class MobEvadeHelper
    {
        private int attackMobSince = -1;
        private ulong lastMobGuid = 0L;
        private uint timeOut = 3000;


        internal MobEvadeHelper()
        {
            WoWEventHandler.Instance.OnErrorMessage += IsMobBugged;

        }

        ~MobEvadeHelper()
        {
            WoWEventHandler.Instance.OnErrorMessage += IsMobBugged;
        }

        internal void IsMobBugged(object n, WoWEventHandler.OnUiMessageArgs e)
        {
            var target = ObjectManager.Instance.Target;
            if (target == null) return;

            if (e.Message.StartsWith("Target out of range"))
            {
                if (lastMobGuid == target.Guid)
                {
                    if (attackMobSince == -1)
                    {
                        attackMobSince = Environment.TickCount;
                    }
                    else
                    {
                        if (Environment.TickCount - attackMobSince > timeOut)
                        {
                            Grinder.Access.PrintToChat("MobEvade: Blacklisting mob.");
                            Grinder.Access.Info.Combat.AddToBlacklist(target.Guid);
                        }
                    }
                }
                else
                {
                    lastMobGuid = target.Guid;
                }
            }
        }

        internal void Reset()
        {
            attackMobSince = -1;

        }
    }
}

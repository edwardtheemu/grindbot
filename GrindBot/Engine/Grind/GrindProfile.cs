﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using GrindBot.Constants;
using GrindBot.Objects;
using ZzukBot.Helpers.PPather;
using ZzukBot.Objects;

namespace GrindBot.Engine.Grind
{
    internal class GrindProfile
    {
        #region constructor

        internal GrindProfile(string parProfilePath)
        {
            try
            {
                // open the supplied xml

                var profileArr = File.ReadAllLines(parProfilePath);
                var profile = profileArr.Aggregate("", (current, t) => current + t.TrimStart('\t', ' ') + Environment.NewLine);

                var doc = XDocument.Parse(profile);
                var sub = doc.Element("Profile");
                // get subelements of xml profile
                var tmpHotspots = sub.Element("Hotspots");
                var tmpVendorHotspots = sub.Element("VendorHotspots");
                var tmpGhostrHotspots = sub.Element("GhostHotspots");
                var tmpMinLevel = sub.Element("MinLevel");
                var tmpMaxLevel = sub.Element("MaxLevel");
                var tmpFactions = sub.Element("Factions");
                var tmpIds = sub.Element("Ids");
                var tmpVendor = sub.Element("Vendor");
                var tmpRepair = sub.Element("Repair");
                var tmpRestock = sub.Element("Restock");
                var tmpRestockItems = sub.Element("RestockItems");

                ExtractHotspots(tmpHotspots);
                // parse all faction ids if avaible
                ExtractFactions(tmpFactions);
                // parse all ids if available
                ExtractIds(tmpIds);
                // parse the vendor
                ExtractVendor(tmpVendor);
                // parse the Repair
                ExtractRepairNpcAndWaypoints(tmpVendorHotspots, tmpRepair);
                // parse the Restock
                ExtractRestockNpcAndItems(tmpRestock, tmpRestockItems);
                ExtractGhosthotspots(tmpGhostrHotspots);
                ExtractMinLevel(tmpMinLevel);
                ExtractMaxLevel(tmpMaxLevel);
                // the profile is valid
                ProfileValid = true;
            }
            catch
            {
                // the profile is invalid
                ProfileValid = false;
            }
        }

        private void ExtractRestockNpcAndItems(XElement tmpRestock, XElement tmpRestockItems)
        {
            if (tmpRestock != null)
            {
                Location vec3 = new Location(0f, 0f, 0f);


                RestockNPC = new Structs.NPC(tmpRestock.Element("Name").Value,
                    vec3, "");


                // parse the Restockitems
                if (tmpRestockItems != null)
                {
                    RestockItems = (from x in tmpRestockItems.Nodes()
                                    select x as XElement
                        into tmpX
                                    where tmpX.Name == "Item"
                                    select new Structs.RestockItem
                                    {
                                        Item = tmpX.Element("Name").Value,
                                        RestockUpTo = Convert.ToInt32(tmpX.Element("RestockUpTo").Value)
                                    }).ToArray();
                }
            }
        }

        private void ExtractGhosthotspots(XElement tmpGhostHotspots)
        {
            if (tmpGhostHotspots != null)
            {
                var tmpListGhostHotspots = new List<Waypoint>();
                // ReSharper disable once LoopCanBeConvertedToQuery
                foreach (var x in tmpGhostHotspots.Nodes().ToList())
                {
                    var tmpX = x as XElement;
                    if (tmpX.Name == "GhostHotspot")
                    {
                        var _vec3 = new Location(

                            Convert.ToSingle(tmpX.Element("X").Value),
                            Convert.ToSingle(tmpX.Element("Y").Value),
                            Convert.ToSingle(tmpX.Element("Z").Value)
                            );

                        var tmpWp = new Waypoint
                        {
                            Position = _vec3,
                            Type = (Enums.PositionType)
                                Enum.Parse(typeof(Enums.PositionType), tmpX.Element("Type").Value, true)
                        };

                        tmpListGhostHotspots.Add(tmpWp);
                    }
                }
                GhostHotspots = tmpListGhostHotspots.ToArray();
            }
        }

        private void ExtractRepairNpcAndWaypoints(XElement tmpVendorHotspots, XElement tmpRepair)
        {
            if (tmpRepair != null)
            {
                if (tmpVendorHotspots != null)
                {
                    var tmpListVendorHotspots = new List<Waypoint>();
                    // ReSharper disable once LoopCanBeConvertedToQuery
                    foreach (var x in tmpVendorHotspots.Nodes().ToList())
                    {
                        var tmpX = x as XElement;
                        if (tmpX.Name == "VendorHotspot")
                        {
                            var _vec3 = new Location(
                                Convert.ToSingle(tmpX.Element("X").Value),
                                Convert.ToSingle(tmpX.Element("Y").Value),
                                Convert.ToSingle(tmpX.Element("Z").Value)
                                );

                            var tmpWp = new Waypoint
                            {
                                Position = _vec3,
                                Type = (Enums.PositionType)
                                    Enum.Parse(typeof(Enums.PositionType), tmpX.Element("Type").Value, true)
                            };

                            tmpListVendorHotspots.Add(tmpWp);
                        }
                    }
                    VendorHotspots = tmpListVendorHotspots.ToArray();
                }

                var vec3 = new Location(

                    Convert.ToSingle(
                        tmpRepair.Element("Position").Element("X").Value),
                    Convert.ToSingle(
                        tmpRepair.Element("Position").Element("Y").Value),
                    Convert.ToSingle(
                        tmpRepair.Element("Position").Element("Z").Value)
                );

                RepairNPC = new Structs.NPC(tmpRepair.Element("Name").Value,
                    vec3, "");
            }
        }

        private void ExtractVendor(XElement tmpVendor)
        {
            if (tmpVendor != null)
            {
                var vec3 = new Location(

                    Convert.ToSingle(
                        tmpVendor.Element("Position").Element("X").Value),
                    Convert.ToSingle(
                        tmpVendor.Element("Position").Element("Y").Value),
                    Convert.ToSingle(
                        tmpVendor.Element("Position").Element("Z").Value)
                );

                VendorNPC = new Structs.NPC(tmpVendor.Element("Name").Value,
                    vec3, "");
            }
        }

        private void ExtractFactions(XElement tmpFactions)
        {
            if (tmpFactions != null)
            {
                var tmpListFactions = new List<int>();
                // ReSharper disable once LoopCanBeConvertedToQuery
                foreach (var x in tmpFactions.Nodes().ToList())
                {
                    var tmpX = x as XElement;
                    if (tmpX.Name == "Faction")
                        tmpListFactions.Add(Convert.ToInt32(tmpX.Value));

                }
                Factions = tmpListFactions.ToArray();
            }
        }

        private void ExtractIds(XElement tmpIds)
        {
            if (tmpIds != null)
            {
                var tmpListIds = new List<int>();
                // ReSharper disable once LoopCanBeConvertedToQuery
                foreach (var x in tmpIds.Nodes().ToList())
                {
                    var tmpX = x as XElement;
                    if (tmpX.Name == "Id")
                        tmpListIds.Add(Convert.ToInt32(tmpX.Value));

                }
                Ids = tmpListIds.ToArray();
            }
        }

        private void ExtractMinLevel(XElement tmpMinLevel)
        {
            if (tmpMinLevel != null)
            {
                MinLevel = Convert.ToInt32(tmpMinLevel.Value);
            }
        }

        private void ExtractMaxLevel(XElement tmpMaxLevel)
        {
            if (tmpMaxLevel != null)
            {
                MaxLevel = Convert.ToInt32(tmpMaxLevel.Value);
            }
        }

        private void ExtractHotspots(XElement tmpHotspots)
        {
            // Parse all hotspots
            var tmpListHotspots = new List<Waypoint>();
            // ReSharper disable once LoopCanBeConvertedToQuery
            foreach (var x in tmpHotspots.Nodes().ToList())
            {
                var tmpX = x as XElement;
                if (tmpX.Name == "Hotspot")
                {
                    var vec3 = new Location(

                        Convert.ToSingle(tmpX.Element("X").Value),
                        Convert.ToSingle(tmpX.Element("Y").Value),
                        Convert.ToSingle(tmpX.Element("Z").Value)
                    );

                    var tmpWp = new Waypoint
                    {
                        Position = vec3,
                        Type =
                            (Enums.PositionType)
                                Enum.Parse(typeof(Enums.PositionType), tmpX.Element("Type").Value, true)
                    };

                    tmpListHotspots.Add(tmpWp);
                }
            }
            Hotspots = tmpListHotspots.ToArray();
            OriginalHotspots = Hotspots;
        }

        #endregion

        #region fields

        // to see if the supplied profile in the constructor is valid
        internal bool ProfileValid { get; private set; }
        // Holding the hotspots of the currently loaded profile
        internal Waypoint[] Hotspots { get; set; }
        internal Waypoint[] OriginalHotspots { get; private set; }

        // Holding the vendor hotspots of the currently loaded profile
        internal Waypoint[] VendorHotspots { get; private set; }

        internal Waypoint[] GhostHotspots { get; private set; }
        // Holding all factions to kill
        internal int[] Factions { get; private set; }
        // Holding all ids to kill
        internal int[] Ids { get; private set; }
        // Holding Min Level
        internal int MinLevel { get; private set; }
        // Holding Max Level
        internal int MaxLevel { get; private set; }
        // Vendor infos
        internal Structs.NPC VendorNPC { get; private set; }
        // Repair infos
        internal Structs.NPC RepairNPC { get; private set; }
        // Restock infos
        internal Structs.NPC RestockNPC { get; private set; }
        // Items to restock
        internal Structs.RestockItem[] RestockItems { get; private set; }

        #endregion
    }
}
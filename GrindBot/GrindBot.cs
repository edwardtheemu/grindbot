﻿using System;
using System.ComponentModel.Composition;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Windows.Forms;
using GrindBot.Engine;
using GrindBot.Gui;
using GrindBot.Settings;
using ZzukBot.Game.Statics;


namespace GrindBot
{
    [Export(typeof(ZzukBot.ExtensionFramework.Interfaces.IBotBase))]
    public class GrindBot : ZzukBot.ExtensionFramework.Interfaces.IBotBase
    {
        private string _name = "GrindBot";
        private string _author = "Zzuk / Emu";
        private int _version = 13;

        private Form _guiForm = new Main(null, 0);
        public bool Start(Action onStopCallback)
        {
            //Setup Paths
            var strPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            Paths.ProfileFolder = Path.GetDirectoryName(strPath) + "\\Botbases\\GrindbotProfiles";

            //We don't want the GUI to show unless we can start!
            if (!ZzukBot.Game.Statics.ObjectManager.Instance.IsIngame) return false;

            _guiForm.Dispose();
            _guiForm = new Main(onStopCallback, Version);
            _guiForm.Show(); 

            return true; 
        }

        public void Stop()
        {
            Manager.StopCurrentEngine();
            _guiForm.Close();
            _guiForm.Dispose();
        }

        public void ShowGui()
        {
            if (_guiForm == null)
            {
                MessageBox.Show("Start the bot first!");
            }
            else
            {
                _guiForm.Show();
            } 
        }

        public void _Dispose()
        {
            this.Stop();
        }

        public string Name
        {
            get
            { return _name; }
        }

        public string Author
        {
            get { return _author; }
        }
        public int Version
        {
            get { return _version; }
        }
    }
}

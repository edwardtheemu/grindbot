﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrindBot.Constants
{
    internal static class Enums
    {
        internal enum WaypointType
        {
            Waypoint = 0,
            VendorWaypoint = 1,
            GhostWaypoint = 2
        }

        internal enum PositionType
        {
            Hotspot,
            Waypoint
        }

        internal enum CtmType
        {
            FaceTarget = 0x1,
            Face = 0x2,
            Stop = 0x3,
            Move = 0x4,
            NpcInteract = 0x5,
            Loot = 0x6,
            ObjInteract = 0x7,
            FaceOther = 0x8,
            Skin = 0x9,
            AttackPosition = 0xA,
            AttackGuid = 0xB,
            ConstantFace = 0xC,
            None = 0xD,
            Attack = 0x10,
            Idle = 0xC
        }

        internal enum MovementFlags : uint
        {
            None = 0x0,
            Front = 0x00000001,
            Back = 0x00000002,
            Left = 0x00000010,
            Right = 0x00000020,
            StrafeLeft = 0x00000004,
            StrafeRight = 0x00000008,

            Swimming = 0x00200000,
            jumping = 0x00002000,
            Falling = 0x0000A000,
            Levitate = 0x70000000
        }

        internal enum ItemQuality
        {
            Grey = 0,
            White = 1,
            Green = 2,
            Blue = 3,
            Epic = 4
        }

        internal enum SearchType
        {
            All = 0,
            Faction,
            Id,
            Both
        }

    }
}

﻿using ZzukBot.Helpers.PPather;
using GrindBot.Constants;

namespace GrindBot.Objects
{

    internal class Waypoint
    {
        internal Location Position;
        internal Enums.PositionType Type;

        internal Waypoint()
        {
        }

        internal Waypoint(Location parPos, Enums.PositionType parPosType)
        {
            Position = parPos;
            Type = parPosType;
        }
    }
}
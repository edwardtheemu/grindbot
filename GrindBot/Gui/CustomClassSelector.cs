﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZzukBot.ExtensionFramework;

namespace GrindBot.Gui
{
    public partial class CustomClassSelector : Form
    {

        private Dictionary<string, int> ClassDictionary;
        public CustomClassSelector(List<int> ccs)
        {
            InitializeComponent();
            ClassDictionary = new Dictionary<string, int>();
            foreach (var i in ccs)
            {
                ccLst.Items.Add(CustomClasses.Instance.Enumerator.ElementAt(i).Name);
                ClassDictionary.Add(CustomClasses.Instance.Enumerator.ElementAt(i).Name, i);
            }
        }

        private void selectBtn_Click(object sender, EventArgs e)
        {
            //Check the user has exactly 1 item selected
            if (!ccLst.SelectedIndex.Equals(null) && ccLst.SelectedItems.Count == 1)
            {
                //Set the selected CC (Using position stored in dictionary)
                CustomClasses.Instance.SetCurrent(ClassDictionary[ccLst.Items[ccLst.SelectedIndex].ToString()]);
                //Force the form to close
                this.Dispose();
            }
        }
    }
}
